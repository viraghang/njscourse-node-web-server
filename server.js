const path = require('path')
const express = require('express')
const hbs = require('hbs')
const fs = require('fs')

const port = process.env.PORT || 3000

let app = express()

hbs.registerPartials(path.join(__dirname, '/views/partials'))
app.set('view engine', 'hbs')

app.use((req, res, next) => {
  let now = new Date().toString()
  let log = `${now}: ${req.method} ${req.originalUrl}\n`
  fs.appendFile('server.log', log, err => {
    if (err) console.log(err.message)
  })
  next()
})

// app.use((req, res, next) => {
//   res.render('maintenance.hbs', {
//     pageTitle: 'Maintenance page'
//   })
// })

app.use(express.static(path.join(__dirname, '/public')))

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear()
})

hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase()
})

app.get('/', (req, res) => {
  // res.send('<h1>Hello Express!</h1>')
  // res.send({
  //   errorMessage: 'Error MSG'
  // })

  res.render('index.hbs', {
    pageTitle: 'Main Page',
    welcomeMessage: 'Welcome to my Express sample site'
  })
})

app.get('/about', (req, res) => {
  res.render('about.hbs', {
    pageTitle: 'About Page'
  })
})

app.get('/projects', (req, res) => {
  res.render('projects.hbs', {
    pageTitle: 'Projects Page'
  })
})

app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'This is an error message.'
  })
})

app.listen(port, () => {
  console.log(`Server is up on port ${port}`)
})
